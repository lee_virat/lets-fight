var heart = [];
var heartAtt = [];
var laserposition;
var checklaser;
var numchecklaser;
var star = [] ;
var Score ;
var laser = [];
var heartposition ;
var heartAttposition;
var numheart ;
var axisx1 ,axisx2 ,axisx3 ,axisx4 ;
var hpboss ;
var gunbullet = [] ;
var gunbulletpos;
var get1,get2,get3;
var Countscore = 0;
var GameLayer = cc.LayerColor.extend({
  init: function() {
    Countscore++;
    this._super( new cc.Color( 127, 127, 127, 255 ) );
    this.setPosition( new cc.Point( 0, 0 ) );
    HP =1500;
    heartposition = 650;
    heartAttposition = 650;
    numheart = 0;
    numchecklaser=0;
    checklaser=0;
    Score = 0;
    hpboss = 3;
    axisx1 = 200,axisx2 = 200,axisx3 = 200,axisx4 = 1200;
    gunbulletpos = 680;
    get1=0,get2=0,get3=0;

    cc.audioEngine.playEffect( 'res/sound/soundstart.mp3' );

    cc.audioEngine.playMusic( 'res/sound/soundgame2.wav', true );


        this.brick = new Background_2();
        this.brick.setPosition(cc.p(400,200));
        this.brick.scheduleUpdate();
        this.addChild(this.brick);


        this.startstate = new Background();
        this.startstate.setPosition(cc.p(400,480));
        this.startstate.scheduleUpdate();
        this.addChild(this.startstate);

        this.stick = new stick();
        this.stick.setPosition(cc.p(1100,440));
        this.stick.scheduleUpdate();
        this.addChild(this.stick);

        this.stick2 = new stick2();
        this.stick2.setPosition(cc.p(1500,440));
        this.stick2.scheduleUpdate();
        this.addChild(this.stick2);

        this.stick3 = new stick3();
        this.stick3.setPosition(cc.p(1250,100));
        this.stick3.scheduleUpdate();
        this.addChild(this.stick3);


       
        


        for(var i = 0 ; i <= 26 ; i++){
        star[i] = new Star();
        }
        
        for(var run = 0 ; run <=7 ; run ++){
            
                 star[run].setPosition(cc.p(axisx1,300));
                 star[run].scheduleUpdate();
                 this.addChild(star[run]);              
        axisx1 += 100 ;
        }
         for(var run = 8 ; run <=15 ; run ++){
            
                 star[run].setPosition(cc.p(axisx2,180));
                 star[run].scheduleUpdate();
                 this.addChild(star[run]);              
        axisx2 += 100 ;
        }
         for(var run = 16 ; run <=23 ; run ++){
            
                 star[run].setPosition(cc.p(axisx3,60));
                 star[run].scheduleUpdate();
                 this.addChild(star[run]);              
        axisx3 += 100 ;
        }
          for(var run = 24 ; run <= 26 ; run ++){
            
                 star[run].setPosition(cc.p(axisx4,180));
                 star[run].scheduleUpdate();
                 this.addChild(star[run]);              
        axisx4 += 300 ;
        }

        
         this.bullet = new Bullet();
         this.bullet.randomPosition();
         this.bullet.scheduleUpdate();
        this.addChild(this.bullet);

         this.bullet2 = new Bullet_2();
         this.bullet2.randomPosition();
         this.bullet2.scheduleUpdate();
        this.addChild(this.bullet2);

         this.bullet3 = new Bullet_3();
         this.bullet3.randomPosition();
         this.bullet3.scheduleUpdate();
        this.addChild(this.bullet3);

         this.bullet4 = new Bullet_4();
         this.bullet4.randomPosition();
         this.bullet4.scheduleUpdate();
        this.addChild(this.bullet4);

          this.bullet5 = new Bullet_5();
         this.bullet5.randomPosition();
         this.bullet5.scheduleUpdate();
        this.addChild(this.bullet5);


        for(var i = 0 ; i <= 2 ; i++){
            laser[i] = new Laser();
             laser[i].setPosition(cc.p(1500,0))
            laser[i].scheduleUpdate();
            this.addChild(laser[i]);
        }
        

        this.monster = new Monster();
        this.monster.setPosition(cc.p(1400,220));
        this.monster.scheduleUpdate();
        this.addChild(this.monster);



        this.Player = new Player();
        this.Player.setPosition( cc.p( 60, 100 ) );
        this.Player.scheduleUpdate();
        this.addChild( this.Player);

        this.zombieup = new Zombieup();
        this.zombieup.setPosition( cc.p( 60, 900 ) );
        this.zombieup.scheduleUpdate();
        this.addChild( this.zombieup);



       for(var i = 0 ; i <= 2 ; i++ ){
        heartAtt[i] = new HeartAtt();
        heart[i] = new Heart();
        heart[i].setPosition(cc.p(heartposition, 550 ));
        heart[i].scheduleUpdate();
        this.addChild(heart[i]);
        heartposition += 50;
       }


       this.gun = new Gun();
       this.gun.setPosition(cc.p(600, 450));
       this.gun.scheduleUpdate();
       this.addChild(this.gun);

       for(var i = 1 ; i <= 3 ; i++){
        gunbullet[i] = new Gunbullet();
        gunbullet[i].setPosition(cc.p(gunbulletpos , 450));
        gunbullet[i].scheduleUpdate();
        this.addChild(gunbullet[i]);
        gunbulletpos += 30;
       }






    this.Scoreboard = cc.LabelTTF.create( '100', 'Arial', 40 );
    this.Scoreboard.setPosition( new cc.Point( 400, 550 ) );
    this.addChild( this.Scoreboard );  

    
        this.scheduleUpdate();
        this.addKeyboardHandlers();
       

    return true;
  },

    update: function() {
   
    if(this.monster.closeTo(this.Player)){

        heartAtt[numheart].setPosition(cc.p(heartAttposition, 550 ));
        heartAtt[numheart].scheduleUpdate();
        this.addChild(heartAtt[numheart]);
        this.removeChild(heart[numheart])
        this.monster.hitmonster();
         numheart++;
         heartAttposition += 50;

       if(numheart == 3){
         cc.director.runScene(new deadmenuScene());
        cc.audioEngine.playEffect( 'res/sound/soundend.mp3' );
        cc.audioEngine.playMusic( 'res/sound/soundgame2.wav', false );
       }
    }


    if ( this.bullet.closeTo( this.Player ) || this.bullet2.closeTo(this.Player)) {
       
       
        heartAtt[numheart].setPosition(cc.p(heartAttposition, 550 ));
        heartAtt[numheart].scheduleUpdate();
        this.addChild(heartAtt[numheart]);
        this.removeChild(heart[numheart])


        if(this.bullet.closeTo( this.Player )){
           this.bullet.randomPosition();
           cc.audioEngine.playEffect( 'res/sound/soundzombiehit.mp3' );
        }
       else{
        this.bullet2.randomPosition();
        cc.audioEngine.playEffect( 'res/sound/soundzombiehit.mp3' );
       }
        

     

    numheart++;
    heartAttposition += 50;

       if(numheart == 3){
         cc.director.runScene(new deadmenuScene());
        cc.audioEngine.playEffect( 'res/sound/soundend.mp3' );
        cc.audioEngine.playMusic( 'res/sound/soundgame2.wav', false );
       }
    

    }
    if ( this.bullet3.closeTo( this.Player ) || this.bullet4.closeTo(this.Player)||this.bullet5.closeTo(this.Player)) {


        heartAtt[numheart].setPosition(cc.p(heartAttposition, 550 ));
        heartAtt[numheart].scheduleUpdate();
        this.addChild(heartAtt[numheart]);
        this.removeChild(heart[numheart])
       


       

        if(this.bullet3.closeTo( this.Player )){
            this.bullet3.randomPosition();
            cc.audioEngine.playEffect( 'res/sound/soundzombiehit.mp3' );
        }
             
        else if(this.bullet4.closeTo( this.Player )){
              this.bullet4.randomPosition();
            cc.audioEngine.playEffect( 'res/sound/soundzombiehit.mp3' );
        }
           
        else if(this.bullet5.closeTo(this.Player)){
             this.bullet5.randomPosition();
            cc.audioEngine.playEffect( 'res/sound/soundzombiehit.mp3' );
        }
        


         numheart++;
         heartAttposition += 50;

          if(numheart == 3){
             cc.director.runScene(new deadmenuScene());
             cc.audioEngine.playEffect( 'res/sound/soundend.mp3' );
             cc.audioEngine.playMusic( 'res/sound/soundgame2.wav', false );
          }
    }

    for(var i = 0 ; i <= 2 ; i++){

        if(laser[i].closeTo(this.monster)){
            hpboss--;
            laser[i].laserhit();
        }
        if(hpboss==0){
            this.monster.hitmonster();
            cc.audioEngine.playEffect( 'res/sound/soundmonter.mp3' );
             if(this.monster.getPosition().x == 670)
                Score += 500;

            hpboss = 3;
        }
        if(laser[i].closeTo(this.bullet)||laser[i].closeTo(this.bullet2)||laser[i].closeTo(this.bullet3)||laser[i].closeTo(this.bullet4)||laser[i].closeTo(this.bullet5)){
            if(this.bullet.closeTo( laser[i] )){
                cc.audioEngine.playEffect( 'res/sound/soundatt.mp3' );
                laser[i].laserhit();
                this.bullet.randomPosition();
            }         
            if(this.bullet2.closeTo( laser[i] )){
                cc.audioEngine.playEffect( 'res/sound/soundatt.mp3' );
                laser[i].laserhit();
                this.bullet2.randomPosition();
            } 
            if(this.bullet3.closeTo( laser[i] )){
                cc.audioEngine.playEffect( 'res/sound/soundatt.mp3' );
                laser[i].laserhit();
                this.bullet3.randomPosition();
            } 
            if(this.bullet4.closeTo( laser[i] )){
                cc.audioEngine.playEffect( 'res/sound/soundatt.mp3' );
                laser[i].laserhit();
                this.bullet4.randomPosition();
            } 
            if(this.bullet5.closeTo( laser[i] )){
                cc.audioEngine.playEffect( 'res/sound/soundatt.mp3' );
                laser[i].laserhit();
                this.bullet5.randomPosition();
            } 

        Score += 50;
        
    
    }
    }

   
     


    for(var i = 24 ; i <= 26 ; i++ ){
        if(star[i].getPosition().x <= - 20)
             star[i].randomPosition();
         if(star[i].closeTo(this.Player)){
            star[i].randomPosition();
            Score += 100;
        }
    }

    for(var i = 0 ; i <= 23 ;i++){
        if(star[i].closeTo(this.Player)){
            Score += 100;
            star[i].disappear();
        }
    }

    if(laser[0].getPosition().x>=840&&laser[1].getPosition().x>=840&&laser[2].getPosition().x >= 840&&numchecklaser==3){
        gunbulletpos = 680;
        for(var i = 1 ; i <= 3 ; i++){
        gunbullet[i] = new Gunbullet();
        gunbullet[i].setPosition(cc.p(gunbulletpos , 450));
        gunbullet[i].scheduleUpdate();
        this.addChild(gunbullet[i]);
        gunbulletpos += 30;
       }
    }

    if(get1 == 5 || get2 == 5 || get3 == 5){
        
        speedzombieup = 5 ;
         cc.audioEngine.playEffect( 'res/sound/zombieup.wav' );
       // if(this.zombieup.closeTo( this.Player )){
        heartAtt[numheart].setPosition(cc.p(heartAttposition, 550 ));
        heartAtt[numheart].scheduleUpdate();
        this.addChild(heartAtt[numheart]);
        this.removeChild(heart[numheart])

         numheart++;
         heartAttposition += 50;


       if(numheart == 3){
         cc.director.runScene(new deadmenuScene());
        cc.audioEngine.playEffect( 'res/sound/soundend.mp3' );
        cc.audioEngine.playMusic( 'res/sound/soundgame2.wav', false );
       }
    // }

        get1 = 0;
        get2 = 0;
        get3 = 0; 


    }



   
    this.Scoreboard.setString(Score);
    this.resetLaser();

    },

    resetLaser: function(){
       

        if(laser[0].getPosition().x>=840&&laser[1].getPosition().x>=840&&laser[2].getPosition().x>=840&&numchecklaser==3){
            numchecklaser=0;
        }
            
        
        

    },

  onKeyDown: function( keyCode, event ) {
   if ( keyCode == cc.KEY.up ) {
        this.Player.switchDirection(cc.KEY.up);
       
    }
    if ( keyCode == cc.KEY.down ) {
      this.Player.switchDirection(cc.KEY.down);
       }

    if(keyCode == cc.KEY.space){

        

        if(numchecklaser<=2&&numchecklaser>=0){

         numchecklaser++;

         this.removeChild(gunbullet[numchecklaser]);
   
         if(this.Player.getPosition().y == 100){
            if(numchecklaser==1){
                 laser[0].laserout(1);
                 get1++;
                 get2--;
                 get3--;
                if(get1<0)
                    get1=0;
                if(get2<0)
                    get2=0;
                if(get3<0)
                    get3=0;


            }
             if(numchecklaser==2){
                 laser[1].laserout(1);
                 get1++;
                 get2--;
                 get3--;
                 if(get1<0)
                    get1=0;
                if(get2<0)
                    get2=0;
                if(get3<0)
                    get3=0;
            }
             if(numchecklaser==3){
                 laser[2].laserout(1);
                 get1++;
                 get2--;
                 get3--;
                 if(get1<0)
                    get1=0;
                if(get2<0)
                    get2=0;
                if(get3<0)
                    get3=0;
            }
           
    }
       if(this.Player.getPosition().y == 220){
              if(numchecklaser==1){
                 laser[0].laserout(2);
                 get2++;
                 get1--;
                 get3--;
                 if(get1<0)
                    get1=0;
                if(get2<0)
                    get2=0;
                if(get3<0)
                    get3=0;
            }
             if(numchecklaser==2){
                 laser[1].laserout(2);
                 get2++;
                 get1--;
                 get3--;
                if(get1<0)
                    get1=0;
                if(get2<0)
                    get2=0;
                if(get3<0)
                    get3=0;
            }
             if(numchecklaser==3){
                 laser[2].laserout(2);
                 get2++;
                 get1--;
                 get3--;
                 if(get1<0)
                    get1=0;
                if(get2<0)
                    get2=0;
                if(get3<0)
                    get3=0;
            }
    }
       if(this.Player.getPosition().y == 340){
            if(numchecklaser==1){
                 laser[0].laserout(3);
                 get3++;
                 get2--;
                 get1--;
                 if(get1<0)
                    get1=0;
                if(get2<0)
                    get2=0;
                if(get3<0)
                    get3=0;
                
            }
             if(numchecklaser==2){
                 laser[1].laserout(3);
                 get3++;
                 get2--;
                 get1--;
                 if(get1<0)
                    get1=0;
                if(get2<0)
                    get2=0;
                if(get3<0)
                    get3=0;
                
            }
             if(numchecklaser==3){
                 laser[2].laserout(3);
                 get3++;
                 get2--;
                 get1--;
                 if(get1<0)
                    get1=0;
                if(get2<0)
                    get2=0;
                if(get3<0)
                    get3=0;
               
            }
    }
       
}


      
    }
   
  },
 onKeyUp: function( keyCode, event ) {
        if ( keyCode == cc.KEY.up ) {
        this.Player.switchDirection(cc.KEY.right);
        
    }
    if ( keyCode == cc.KEY.down ) {
      this.Player.switchDirection(cc.KEY.right);
       
    }
    },

   addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    }




});

var StartScene = cc.Scene.extend({
  onEnter: function() {
    this._super();
    var layer = new GameLayer();
    layer.init();
    this.addChild( layer );
  }
});


