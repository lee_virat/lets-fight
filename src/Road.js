var speed ;
var Road = cc.Node.extend({
    ctor: function() {
        this._super();
        speed = 2;
        this.WIDTH = 21;
        this.HEIGHT = 8;
        this.MAP = [
            '#.#.#.#.#.#.#.#.#.#.#',
            '....................',
            '....................',
            '#.#.#.#.#.#.#.#.#.#.#',
            '....................',
            '....................',
            '#.#.#.#.#.#.#.#.#.#.#',
            '....................'
        ];
 
        // ...  code for drawing the maze has be left out
    for ( var r = 0; r < this.HEIGHT; r++ ) {
        for ( var c = 0; c < this.WIDTH; c++ ) {
        if ( this.MAP[ r ][ c ] == '#' ) {
            var s = cc.Sprite.create( 'res/images/Road.png' );
            s.setAnchorPoint( cc.p( 0, 0 ) );
            s.setPosition( cc.p( c * 40, (this.HEIGHT - r - 1) * 40 ) );
            this.addChild( s );
        }
        }
    }

    this.setAnchorPoint( cc.p( 0, 0 ) );

    },

      update: function( dt ) {
        this.setPositionX( this.getPositionX() - speed );
        if(this.getPositionX()<-78){
            this.setPositionX(0,40);
            
            if(speed < 10)
                speed += 0.05;
            else if(speed == 10)
                speed = 10;
        }
    }



});