var zombie = [];
var Deadmenu = cc.LayerColor.extend({
    init:function()
    {
         
       
        this.startstate = new Background_4();
        this.startstate.setPosition(cc.p(400,300));
        this.startstate.scheduleUpdate();
        this.addChild(this.startstate);


        this.item = cc.LabelTTF.create( '100', 'Arial', 30 );
        this.item.setPosition( new cc.Point( screenWidth/2, screenHeight/2-150) );
        this.addChild( this.item);  
        this.item.setString("Spacebar to Playagain");

        this.item2 = cc.LabelTTF.create( '100', 'Arial', 120 );
        this.item2.setPosition( new cc.Point( screenWidth/2, screenHeight/2+150) );
        this.addChild( this.item2);  
        this.item2.setString("Game Over");

        this.item3 = cc.LabelTTF.create( '100', 'Arial', 80 );
        this.item3.setPosition( new cc.Point( screenWidth/2, screenHeight/2) );
        this.addChild( this.item3);  
        this.item3.setString("Score : "+Score);


        for(var i = 0 ; i <= 8 ; i++){
            zombie[i] = new Zombiehand();
            zombie[i].randomPosition();
            zombie[i].scheduleUpdate();
            this.addChild(zombie[i]);
        }
        

        // var menuItem1 = new cc.MenuItemFont.create("Spacebar to Playagain",this,this.playSound);
        // var menuItem2 = new cc.MenuItemFont.create("Play Song",this,this.playSong);
        // var menuItem3 = new cc.MenuItemFont.create("Stop Playing Song",this,this.stopPlayingSound);
        // var menuItem4 = new cc.MenuItemFont.create("Exit",this,this.exit);

        // menuItem1.setPosition(new cc.Point(screenWidth/2,screenHeight/2+50));
        // menuItem2.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
        // menuItem3.setPosition(new cc.Point(screenWidth/2,screenHeight/2-50));
        // menuItem4.setPosition(new cc.Point(screenWidth/2,screenHeight/2-100));

        // var menu = cc.Menu.create(menuItem1);
        // menu.setPosition(new cc.Point(0,0));
        // this.addChild(menu);

        this.scheduleUpdate();
        this.addKeyboardHandlers();
        return true;
    },

   addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            }
           
        }, this);
    },

    onKeyDown: function( keyCode, event ) {
   if ( keyCode == cc.KEY.space ) {
   	  cc.director.runScene(new StartScene());
   	   console.log( 'Upp: ' + keyCode.toString() );
   }
        
    
   
  }


});



deadmenuScene = cc.Scene.extend({
    onEnter:function(){
        this._super();
        var layer = new Deadmenu();
        layer.init();
        this.addChild(layer);
    }
});