var speedlaser;
var Laser = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/Laser.png' );
        speedlaser =4;
    
    },

        closeTo: function( obj ) {
	var myPos = this.getPosition();
	var oPos = obj.getPosition();

  	return ( ( Math.abs( myPos.x - oPos.x ) <= 40 ) &&
		 ( Math.abs( myPos.y - oPos.y ) <= 40 ) );
    },


    laserout: function(num){
    var pos = this.getPosition();
    if(num ==1){
        this.setPosition(cc.p(60,100));
    }
    	
  	if(num == 2){
        this.setPosition(cc.p(60,220));
    }
  	
  	if(num == 3){
      this.setPosition(cc.p(60,340));
    }
    	   		
    },

    laserhit: function(){
       this.setPosition(cc.p(9000,340));
    },

     update: function( dt ) {
      this.setPositionX( this.getPositionX() + speedlaser );
        if(this.getPositionX() == 820){
           this.setPosition(cc.p(9000,900));
           
        }
         
      
    }
 
});