var res = {
	Road_png: 'res/images/Road.png',
	Playerskate_png: 'res/images/Playerskate.png',
	Bullet_png: 'res/images/bullet.png',
	Player_jpg: 'res/images/Player_jpg',
	blood_jpg: 'res/images/blood_jpg',
	grass_jpg: 'res/images/grass_jpg',

	startsound: 'res/sound/soundstart.mp3',
	endsound: 'res/sound/soundend.mp3',
	zombiesoundhit: 'res/sound/soundzombiehit.mp3',
	attsound: 'res/sound/soundatt.mp3',
	montersound: 'res/sound/soundmonter.mp3',
	gamesound: 'res/sound/soundgame.wav',
	gamesound2: 'res/sound/soundgame2.wav',
	zobieupsound: 'res/sound/zombieup.wav'
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
